package escoladeltreball.org.decaught.utilities;


import escoladeltreball.org.decaught.rest.RetrofitClient;
import escoladeltreball.org.decaught.rest.SOService;

public class ApiUtils {

    public static SOService getSOService() {
        return RetrofitClient.getClient(Statics.getBaseUrl()).create(SOService.class);
    }
}