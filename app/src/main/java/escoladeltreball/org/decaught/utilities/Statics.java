package escoladeltreball.org.decaught.utilities;

import android.Manifest;

public class Statics {
    public static final String PREF_NAME = "decaught-preferences";
   public static final String BASE_URL = "http://10.0.2.2:8000/";
//    public static final String BASE_URL = "http://192.168.1.104:8000/";
//    public static final String BASE_URL = "http://192.168.43.176:8000/";
    /*public static final String BASE_URL = "http://192.168.43.104:8000/";*/
//    public static final String BASE_URL = "http://192.168.1.101:8000/";

    public static final String CHANNEL_ID = "mychannelid";
    public static final String CHANNEL_NAME = "mychannelname";
    public static final String CHANNEL_DESCRIPTION = "mydescription";

    public static final String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE};


    public static String getPrefName() {
        return PREF_NAME;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getChannelId() {
        return CHANNEL_ID;
    }

    public static String getChannelName() {
        return CHANNEL_NAME;
    }

    public static String getChannelDescription() {
        return CHANNEL_DESCRIPTION;
    }

    public static String[] getPERMISSIONS() {
        return PERMISSIONS;
    }
}
