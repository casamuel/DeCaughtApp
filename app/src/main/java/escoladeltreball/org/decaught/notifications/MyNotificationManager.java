package escoladeltreball.org.decaught.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.activities.MasterActivity;
import escoladeltreball.org.decaught.models.GeofenceR;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MyNotificationManager {

    private Context context;
    private static MyNotificationManager mInstance;
    private SOService mService;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public MyNotificationManager(Context context) {
        this.context = context;
    }

    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }

    public void processNotification(String title, String body, Map<String, String> data) {


        if (data.get("action") != null) {
            switch (data.get("action")) {
                case "update_geofences":
                    downloadGeofences();
                    break;
                default:
                    break;

            }
        } else if (data.get("device") != null) {
            displayNotification(title, body, data.get("device"));
        } else {
            displayNotification(title, body, null);
        }
    }


    private void displayNotification(String title, String body, String device) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, Statics.getChannelId())
                .setSmallIcon(R.drawable.ic_announcement_black_24dp)
                .setContentTitle(title)
                .setContentText(body);

        Intent intent = new Intent(context, MasterActivity.class);
        if (device != null) {
            intent.putExtra("DEVICE", device);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(1, mBuilder.build());
        }
    }

    public void downloadGeofences() {

        System.out.println("NIVEL 1: Entrado en el metodo downloadGeofences");
        preferences = context.getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);

        mService = ApiUtils.getSOService();
        String imei = preferences.getString("imei", "");

        mService.listGeofence("Token " + preferences.getString("token", ""), imei).enqueue(new Callback<List<GeofenceR>>() {
            @Override
            public void onResponse(Call<List<GeofenceR>> call, Response<List<GeofenceR>> response) {

                try {
                    FileOutputStream fileOutputStream = context.openFileOutput("GeofenceList.der", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(response.body());
                    objectOutputStream.close();
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<List<GeofenceR>> call, Throwable t) {

            }
        });
    }
}
