package escoladeltreball.org.decaught.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.models.Token;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    TextView register_textview;
    private TextView user;
    private TextView password;
    private Button sign_in_button;
    private SOService mService;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        mService = ApiUtils.getSOService();

        checkPreferences();
        setup();
    }

    private void setup() {
        register_textview = findViewById(R.id.register_textview);
        user = findViewById(R.id.user);
        password = findViewById(R.id.password);
        sign_in_button = findViewById(R.id.sign_in_button);

        sign_in_button.setOnClickListener(v -> signIn());
        register_textview.setOnClickListener(v -> {startActivity(new Intent(this, RegisterActivity.class)); finish();});
    }

    private void checkPreferences() {
        if (!preferences.getString("username", "").isEmpty() && !preferences.getString("password", "").isEmpty() && !preferences.getString("token", "").isEmpty()) {
            if (preferences.getString("imei", "").isEmpty() && preferences.getString("type", "").isEmpty()) {
                startActivity(new Intent(getApplicationContext(), RolSelectionActivity.class));
                finish();
            } else {
                if (preferences.getString("type", "").equals("E")) {
                    startActivity(new Intent(LoginActivity.this, EmitterActivity.class));
                } else if (preferences.getString("type", "").equals("M")) {
                    startActivity(new Intent(LoginActivity.this, MasterActivity.class));
                }
                finish();
            }
        }

    }

    //Magic english

    /***
     * Método para logearse, manda a la base de datos username y contraseña, si existe obtiene un tokken y lo guarda.
     */
    private void signIn() {
        if (validate()) {
            mService.get_tokken(user.getText().toString(), password.getText().toString()).enqueue(new Callback<Token>() {
                @Override
                public void onResponse(Call<Token> call, Response<Token> response) {
                    if (response.isSuccessful()) {
                        editor.putString("username", user.getText().toString());
                        editor.putString("password", password.getText().toString());
                        editor.putString("token", response.body().getToken());
                        editor.apply();
                        startActivity(new Intent(getApplicationContext(), RolSelectionActivity.class));
                        finish();

                    } else {
                        int statusCode = response.code();

                        if (statusCode == 400) {
                            Toast.makeText(LoginActivity.this, getString(R.string.bad_login), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<Token> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Error al conectar", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                    Log.d("MainActivity", "error loading from API");
                }
            });
        }

    }

    /***
     * Método que valida los datos introducidos
     * @return
     */
    private boolean validate() {
        boolean isValid = true;

        if (user.getText().toString().isEmpty()) {
            user.setError(getString(R.string.field_required));
            isValid = false;
        }

        if (password.getText().toString().isEmpty()) {
            password.setError(getString(R.string.field_required));
            isValid = false;
        }

        return isValid;
    }


}
