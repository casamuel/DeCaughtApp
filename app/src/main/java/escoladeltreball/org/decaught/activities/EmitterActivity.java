package escoladeltreball.org.decaught.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.models.Contact;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.services.UbicacionEmisor;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmitterActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {

    private Button sosBtn;
    private TextView direction;
    private CardView card_view;

    private SOService mService;
    private SharedPreferences preferences;

    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleApiClient googleApiClient;
    private Geocoder geocoder;

    private String currentStreet = "La dirección no está disponible.";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emitter);

/*        Intent startGeofenceIntent = new Intent(EmitterActivity.this, GeofenceService.class);
        startService(startGeofenceIntent);*/

        Intent startServiceIntent = new Intent(EmitterActivity.this, UbicacionEmisor.class);
        startService(startServiceIntent);

        mService = ApiUtils.getSOService();
        preferences = getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);

        locationSetup();
        setup();

        getLocation();
    }


    private void locationSetup() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        geocoder = new Geocoder(this, Locale.getDefault());
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }

    private void setup() {
        direction = findViewById(R.id.direction);
        direction.setText(currentStreet);
        card_view = findViewById(R.id.card_view);
        card_view.setOnClickListener(v -> getLocation());
        sosBtn = findViewById(R.id.sos_btn);
        sosBtn.setOnClickListener(v -> sos());
    }

    @SuppressLint("MissingPermission")
    private void sos() {
        getLocation();

        mService.get_contacts("Token " + preferences.getString("token", ""), "M")
                .enqueue(new Callback<List<Contact>>() {
                    @Override
                    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                        String uri = "tel:" + response.body().get(0).getPhone();
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                        Log.v("REST", "Callign master...");
                    }

                    @Override
                    public void onFailure(Call<List<Contact>> call, Throwable t) {
                        Log.e("REST", "Failure on contacts require.");
                        Log.e("REST", t.getMessage());
                    }
                });
    }

    private String getCurrentDirection(Location currentLocation) {
        List<Address> addresses = new ArrayList<>();
        String currentAddress = "La dirección no está disponible.";
        assert currentLocation != null;
        try {
            addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses.size() != 0) {
            Address address = addresses.get(0);
            currentAddress = address.getAddressLine(0);
        }

        return currentAddress;
    }

    private void sendLocation(Location currentLocation) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        String datetime = f.format(new Date());




        mService.add_location("Token " + preferences.getString("token", ""),
                preferences.getString("imei", ""),
                datetime, String.valueOf(currentLocation.getLatitude()),
                String.valueOf(currentLocation.getLongitude()),
                Boolean.valueOf(preferences.getString("im_out", "")))
                .enqueue(new Callback<escoladeltreball.org.decaught.models.Location>() {
                    @Override
                    public void onResponse(Call<escoladeltreball.org.decaught.models.Location> call, Response<escoladeltreball.org.decaught.models.Location> response) {
                        Log.v("REST", "Location correctly send.");
                    }

                    @Override
                    public void onFailure(Call<escoladeltreball.org.decaught.models.Location> call, Throwable t) {
                        Log.e("REST", "Failure on location send.");
                    }
                });
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        currentStreet = getCurrentDirection(location);
                        direction.setText(currentStreet);
                        sendLocation(location);
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


}
