package escoladeltreball.org.decaught.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.models.Contact;
import escoladeltreball.org.decaught.models.Device;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.services.MyFirebaseMessagingService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RolSelectionActivity extends AppCompatActivity {

    private Button button_paciente;
    private Button button_maestro;

    private TelephonyManager mTelephonyManager;
    private String deviceid;
    private SOService mService;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static final String PREF_NAME = "decaught-preferences";
    private static final int PERMISSIONS_MULTIPLE_REQUEST = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rol_selection);

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();

        mService = ApiUtils.getSOService();

        button_paciente = findViewById(R.id.button_paciente);
        button_maestro = findViewById(R.id.button_maestro);

        requestPermissions();


        button_maestro.setOnClickListener(v -> showAddItemDialog(this, "M"));
        button_paciente.setOnClickListener(v -> showAddItemDialog(this, "E"));
    }

    private void requestPermissions() {
        if (!hasPermissions(this, Statics.getPERMISSIONS())) {
            ActivityCompat.requestPermissions(this, Statics.getPERMISSIONS(), PERMISSIONS_MULTIPLE_REQUEST);
        } else {
            getDeviceImei();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                boolean phonePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean locationPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (phonePermission && locationPermission) {
                    getDeviceImei();
                } else {
                    finish();
                }
                return;
            }
            default:
                finish();
        }
    }

    /***Abre una ventana en la que se debe insertar un nombre para el dispositivo, se hará una petición al servidor para añadirlo a la base de datos
     *
     * @param c
     * @param tipo
     */
    private void showAddItemDialog(Context c, String tipo) {


        AlertDialog.Builder alert = new AlertDialog.Builder(RolSelectionActivity.this);
        View alertView = getLayoutInflater().inflate(R.layout.alert_dialog, null);

        //Set the view
        alert.setView(alertView);
        //Show alert
        final AlertDialog alertDialog = alert.show();
        //Can not close the alert by touching outside.
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button addDevice = alertView.findViewById(R.id.add_device);
        Button close = alertView.findViewById(R.id.close_alert_btn);
        EditText device = alertView.findViewById(R.id.device);
        EditText name_contact = alertView.findViewById(R.id.name_contact);
        EditText phone_contact = alertView.findViewById(R.id.phone_contact);

        addDevice.setOnClickListener(v -> {
            String label = String.valueOf(device.getText());
            String name = String.valueOf(name_contact.getText());
            String phone = String.valueOf(phone_contact.getText());
            addDevice(label, tipo, name, phone);
            alertDialog.dismiss();
        });

        close.setOnClickListener(v -> alertDialog.dismiss());
    }


    /***Inserta el contacto asociado con el dispositivo
     *
     * @param name
     * @param phone
     */
    private void addContact(String name, String phone) {
        mService.add_contact("Token " + preferences.getString("token", ""), deviceid, phone, name).enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                if (response.isSuccessful()) {

                    editor.putString("phone", phone);
                    editor.putString("name", name);
                    editor.apply();
                } else {

                    try {
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Toast.makeText(RolSelectionActivity.this, "Error al añadir contacto", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.d("RolSelectionActivity", "error loading from API");
            }
        });


    }

    /***
     *
     * @param etiqueta
     * @param tipo
     * @param name
     * @param phone
     */
    private void addDevice(String etiqueta, String tipo, String name, String phone) {
        String regId = preferences.getString("regId", "");

        mService.add_device("Token " + preferences.getString("token", ""), deviceid, etiqueta, tipo, regId).enqueue(new Callback<Device>() {
            @Override
            public void onResponse(Call<Device> call, Response<Device> response) {
                if (response.isSuccessful()) {
                    addContact(name, phone);
                    MyFirebaseMessagingService.enableNotifications(getApplicationContext());
                    if (tipo.equals("E")) {
                        editor.putString("imei", deviceid);
                        editor.putString("type", "E");
                        editor.apply();
                        startActivity(new Intent(RolSelectionActivity.this, EmitterActivity.class));
                    } else {
                        editor.putString("imei", deviceid);
                        editor.putString("type", "M");
                        editor.apply();
                        startActivity(new Intent(RolSelectionActivity.this, MasterActivity.class));
                    }
                } else {
                    int statusCode = response.code();
                    try {
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println("statusCode: " + statusCode);
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<Device> call, Throwable t) {
                Toast.makeText(RolSelectionActivity.this, "Error al añadir dispositivo", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.d("RolSelectionActivity", "error loading from API");

            }
        });

    }

    /***
     * Método que obtiene el numero IMEI del dispositivo, comprueba si se le ha dado permiso a la aplicación, si no se pide el permiso.
     */
    private void getDeviceImei() {
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
            return;
        }
        deviceid = mTelephonyManager.getDeviceId();

        editor.putString("imei", deviceid);
        editor.apply();

        System.out.println("DEVICE ID: " + deviceid);
        Log.d("msg", "DeviceImei " + deviceid);
    }
}
