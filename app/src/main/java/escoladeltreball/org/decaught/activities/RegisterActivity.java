package escoladeltreball.org.decaught.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.models.Token;
import escoladeltreball.org.decaught.models.User;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private Button registerButton;
    private EditText username_register;
    private EditText last_name_register;
    private EditText mail_register;
    private EditText password_register;
    private EditText repeat_password_register;
    private EditText name_register;

    private SOService mService;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mService = ApiUtils.getSOService();
        preferences = getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        setup();
    }

    private void setup() {
        name_register = findViewById(R.id.name_register);
        registerButton = findViewById(R.id.sign_in_button);
        username_register = findViewById(R.id.username_register);
        last_name_register = findViewById(R.id.last_name_register);
        mail_register = findViewById(R.id.mail_register);
        password_register = findViewById(R.id.password_register);
        repeat_password_register = findViewById(R.id.repeat_password_register);

        registerButton.setOnClickListener(v -> registerUser());
    }


    /***
     * Método que inserta nuevos usuarios
     */
    private void registerUser() {
        boolean isValidate = validate();

        if (isValidate) {
            mService.insert_user(username_register.getText().toString(), name_register.getText().toString(), last_name_register.getText().toString(), mail_register.getText().toString(), password_register.getText().toString()).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {

                        signIn();
                    } else {
                        int statusCode = response.code();
                        System.out.println("statusCode: " + statusCode);
                        // handle request errors depending on status code
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(RegisterActivity.this, "Error al registrar", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                    Log.d("MainActivity", "error loading from API");

                }
            });

        }
    }

    // username_register <-> password_register
    private void signIn() {
        SOService mService = ApiUtils.getSOService();
        mService.get_tokken(username_register.getText().toString(), password_register.getText().toString()).enqueue(
                new Callback<Token>() {
                    @Override
                    public void onResponse(Call<Token> call, Response<Token> response) {
                        if (response.isSuccessful()) {
                            editor.putString("username", username_register.getText().toString());
                            editor.putString("password", password_register.getText().toString());
                            editor.putString("token", response.body().getToken());
                            editor.apply();
                            startActivity(new Intent(getApplicationContext(), RolSelectionActivity.class));
                            finish();
                        } else {
                            int statusCode = response.code();

                            if (statusCode == 400) {
                                Toast.makeText(RegisterActivity.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Token> call, Throwable t) {
                        Toast.makeText(RegisterActivity.this, "Error al conectar", Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        Log.d("MainActivity", "error loading from API");
                    }
                }
        );
    }

    private boolean validate() {

        boolean isValidate = true;

        if (name_register.getText().toString().isEmpty()) {
            name_register.setError("Campo obligatorio");
            isValidate = false;
        }

        if (username_register.getText().toString().isEmpty()) {
            username_register.setError("Campo obligatorio");
            isValidate = false;
        }
        if (last_name_register.getText().toString().isEmpty()) {
            last_name_register.setError("Campo obligatorio");
            isValidate = false;
        }
        if (mail_register.getText().toString().isEmpty()) {
            mail_register.setError("Campo obligatorio");
            isValidate = false;
        }
        if (password_register.getText().toString().isEmpty()) {
            password_register.setError("Campo obligatorio");
            isValidate = false;
        }
        if (repeat_password_register.getText().toString().isEmpty()) {
            repeat_password_register.setError("Campo obligatorio");
            isValidate = false;
        }

        if (!password_register.getText().toString().equals(repeat_password_register.getText().toString()) && (!repeat_password_register.getText().toString().isEmpty() && !password_register.getText().toString().isEmpty())) {
            password_register.setError("Las contraseñas no coinciden");
            repeat_password_register.setError("Las contraseñas no coinciden");
            isValidate = false;
        }

        /*if (android.util.Patterns.EMAIL_ADDRESS.matcher(mail_register.getText().toString()).matches()) {
            mail_register.setError("Email inválido");
            isValidate = false;
        }*/

        return isValidate;
    }

}
