package escoladeltreball.org.decaught.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import escoladeltreball.org.decaught.R;
import escoladeltreball.org.decaught.models.Device;
import escoladeltreball.org.decaught.models.GeofenceR;
import escoladeltreball.org.decaught.models.Location;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import escoladeltreball.org.decaught.utilities.Statics;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterActivity extends FragmentActivity
        implements OnMapReadyCallback {


    private List<Device> allDevices = new ArrayList<>();
    private ArrayList<Location> allLocations = new ArrayList<>();

    private Map<String, String> label_id = new HashMap<>();

    private final static int INTERVAL = 1000 * 60; //1 minutes
    Handler mHandler = new Handler();
    private SOService mService;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private GoogleMap map;
    private Spinner spinner_emiters;
    private String labelSelected;

    private ArrayList<Marker> geoFenceMarker = new ArrayList<>();
    private MarkerOptions lastMarker;

    private FloatingActionButton addSafeZone;

    private Button add_zone_button, close;
    private ConstraintLayout bottomSheet;
    private BottomSheetBehavior bsb;
    private SeekBar seekBar;
    private TextView seekRadius;
    private String device_label;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);

        addSafeZone = findViewById(R.id.addSafeZone);
        bottomSheet = findViewById(R.id.bottomSheet);
        add_zone_button = findViewById(R.id.add_zone_button);
        close = findViewById(R.id.close_btn);
        seekBar = findViewById(R.id.seekBar);
        seekRadius = findViewById(R.id.seek_radius_tv);

        bsb = BottomSheetBehavior.from(bottomSheet);

        Intent intent = getIntent();
        if (intent.hasExtra("DEVICE")) {
            device_label = intent.getStringExtra("DEVICE");
        }

        mService = ApiUtils.getSOService();
        preferences = getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        addItemsOnSpinner2();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekRadius.setText(progress + " metros");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        spinner_emiters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                stopRepeatingTask();

                if (geoFenceMarker != null) {
                    geoFenceMarker.clear();
                }
                lastMarker = null;
                labelSelected = spinner_emiters.getSelectedItem().toString();
                obtainLocations();
                obtainGeofences();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        bsb.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        addSafeZone.setOnClickListener(v -> addZone());
        close.setOnClickListener(v -> closeBottom());
        add_zone_button.setOnClickListener(v -> drawGeofence(seekBar.getProgress() + 150));
    }

    private void obtainGeofences() {

        String imei = label_id.get(labelSelected);


        mService.listGeofence("Token " + preferences.getString("token", ""), imei).enqueue(new Callback<List<GeofenceR>>() {
            @Override
            public void onResponse(Call<List<GeofenceR>> call, Response<List<GeofenceR>> response) {

                assert response.body() != null;
                for (GeofenceR geofence : response.body()) {

                    LatLng latLng = new LatLng(Double.parseDouble(geofence.getLatitude()), Double.parseDouble(geofence.getLongitude()));
                    CircleOptions circleOptions = new CircleOptions()
                            .center(latLng)
                            .strokeColor(Color.argb(50, 6, 74, 98))
                            .fillColor(Color.argb(100, 65, 118, 144))
                            .radius(geofence.getRadius());

                    map.addCircle(circleOptions);
                }
            }

            @Override
            public void onFailure(Call<List<GeofenceR>> call, Throwable t) {

            }
        });
    }


    //Habilita el BottomSheet
    private void addZone() {
        if (lastMarker != null) {
            addSafeZone.hide();
            bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            Toast.makeText(this, "¡Primero añade un marcador!", Toast.LENGTH_SHORT).show();
        }
    }

    private void closeBottom() {
        bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);
        addSafeZone.show();
    }

    private void obtainLocations() {

        startRepeatingTask();

    }


    public void addItemsOnSpinner2() {

        spinner_emiters = findViewById(R.id.spinner_emiters);

        mService.listDevices("Token " + preferences.getString("token", "")).enqueue(new Callback<List<Device>>() {
            @Override
            public void onResponse(Call<List<Device>> call, Response<List<Device>> response) {

                allDevices = response.body();

                showListinSpinner();

                if (device_label != null) {
                    int result = 0;
                    for (int i = 0; i < response.body().size(); i++) {
                        if (response.body().get(i).getDeviceId().equals(device_label)) {
                            result = i;
                        }
                    }
                    spinner_emiters.setSelection(result);
                }
            }

            @Override
            public void onFailure(Call<List<Device>> call, Throwable t) {

            }
        });

    }

    private void showListinSpinner() {
        String[] items = new String[allDevices.size()];

        //Traversing through the whole list to get all the names
        for (int i = 0; i < allDevices.size(); i++) {
            label_id.put(allDevices.get(i).getLabel(), allDevices.get(i).getDeviceId());
            //Storing names to string array
            items[i] = allDevices.get(i).getLabel();
        }

        //Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter;
//        adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1, items);
        adapter = new ArrayAdapter<String>(getApplication(), R.layout.spinner_item, items);
        //setting adapter to spinner
        spinner_emiters.setAdapter(adapter);
        //Creating an array adapter for list view
    }

    @Override
    public void onMapReady(GoogleMap gmap) {

        map = gmap;
        map.setTrafficEnabled(true);

        map.setOnMapClickListener(latLng -> markerForGeofence(latLng));
    }


    private void markerForGeofence(LatLng latLng) {
        // Define marker options
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .title("Zona segura");

        if (map != null) {

            if (geoFenceMarker.size() != 0) {
                geoFenceMarker.get(0).remove();
                geoFenceMarker.remove(0);
            }


            geoFenceMarker.add(map.addMarker(markerOptions));
            lastMarker = markerOptions;
        }
    }


    private void drawGeofence(double geofenceRadius) {


        String imei = label_id.get(labelSelected);

        LatLng marker = lastMarker.getPosition();

        mService.add_geofence("Token " + preferences.getString("token", ""), imei, String.valueOf(marker.latitude), String.valueOf(marker.longitude), String.valueOf(geofenceRadius)).enqueue(new Callback<GeofenceR>() {
            @Override
            public void onResponse(Call<GeofenceR> call, Response<GeofenceR> response) {
                CircleOptions circleOptions = new CircleOptions()
                        .center(geoFenceMarker.get(geoFenceMarker.size() - 1).getPosition())
                        .strokeColor(Color.argb(50, 6, 74, 98))
                        .fillColor(Color.argb(100, 65, 118, 144))
                        .radius(geofenceRadius);

                map.addCircle(circleOptions);

                addSafeZone.show();

                bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            @Override
            public void onFailure(Call<GeofenceR> call, Throwable t) {
                Toast.makeText(MasterActivity.this, "Error al intentar añadir Zona segura", Toast.LENGTH_SHORT).show();
            }
        });


    }


    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {

            obtainLocations();

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }

        private void obtainLocations() {
            map.clear();
            String idSelected = label_id.get(labelSelected);
            mService.lastLocation("Token " + preferences.getString("token", ""), idSelected).enqueue(new Callback<Location>() {
                @Override
                public void onResponse(Call<Location> call, Response<Location> response) {


                    if (response.body() != null) {
                        Location location = response.body();

                        LatLng currentLocation = new LatLng(Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()));
                        MarkerOptions markerOptions = new MarkerOptions().position(currentLocation).title(location.getDevice());

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(currentLocation) // Sets the center of the map
                                .zoom(17) // Sets the zoom
                                .build();
                        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        map.addMarker(markerOptions);
                        obtainGeofences();
                    }

                }

                @Override
                public void onFailure(Call<Location> call, Throwable t) {

                }
            });

        }


    };

    void startRepeatingTask() {
        mHandlerTask.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mHandlerTask);
    }

}
