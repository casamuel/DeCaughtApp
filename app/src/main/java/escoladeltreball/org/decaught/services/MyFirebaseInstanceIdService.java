package escoladeltreball.org.decaught.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import escoladeltreball.org.decaught.utilities.Statics;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        preferences = getSharedPreferences(Statics.getPrefName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        editor.putString("regId", refreshedToken);
        editor.apply();

    }


}
