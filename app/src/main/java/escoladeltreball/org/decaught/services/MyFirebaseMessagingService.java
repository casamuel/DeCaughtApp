package escoladeltreball.org.decaught.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import escoladeltreball.org.decaught.notifications.MyNotificationManager;
import escoladeltreball.org.decaught.utilities.Statics;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (remoteMessage.getNotification().getBody().equals("cosa")) {
            MyNotificationManager.getInstance(getApplicationContext()).processNotification(
                    null,
                    null,
                    remoteMessage.getData());
        } else {
            MyNotificationManager.getInstance(getApplicationContext()).processNotification(
                    remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(),
                    remoteMessage.getData());
        }


    }

    public static void enableNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel =
                    new NotificationChannel(Statics.getChannelId(), Statics.getChannelName(), NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription(Statics.getChannelDescription());
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 300, 1000});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }
}
