package escoladeltreball.org.decaught.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import escoladeltreball.org.decaught.models.GeofenceR;
import escoladeltreball.org.decaught.rest.SOService;
import escoladeltreball.org.decaught.utilities.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbicacionEmisor extends Service {

    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000 * 10;
    private static final float LOCATION_DISTANCE = 0;
    private SOService mService;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static final String PREF_NAME = "decaught-preferences";

    private List<GeofenceR> geofenceRList;


    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);

            DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            Calendar calobj = Calendar.getInstance();



            populateGeofenceList();
            mLastLocation.set(location);

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            String date = f.format(new Date());

            editor.putString("latitude", String.valueOf(location.getLatitude()));
            editor.putString("longitude", String.valueOf(location.getLongitude()));
            editor.apply();


            boolean is_out = true;

            if (geofenceRList != null && geofenceRList.size() != 0) {
                for (GeofenceR geofenceR : geofenceRList) {
                    Location targetLocation = new Location("");//provider name is unnecessary
                    targetLocation.setLatitude(Double.parseDouble(geofenceR.getLatitude()));//your coords of course
                    targetLocation.setLongitude(Double.parseDouble(geofenceR.getLongitude()));
                    if (location.distanceTo(targetLocation) < geofenceR.getRadius()) {
                        is_out = false;
                    }
                }
            } else {
                is_out = false;
            }

            editor.putString("is_out", String.valueOf(is_out));
            editor.apply();

            mService.add_location("Token " + preferences.getString("token", ""), preferences.getString("imei", ""), date, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), is_out).enqueue(new Callback<escoladeltreball.org.decaught.models.Location>() {

                @Override
                public void onResponse(Call<escoladeltreball.org.decaught.models.Location> call, Response<escoladeltreball.org.decaught.models.Location> response) {
                    if (response.isSuccessful()) {
                        Log.d("UbicationEmisor", "Ubication send");
                    } else {

                        if (response.code() == 400) {
                            try {
                                Log.v("Error code 400", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<escoladeltreball.org.decaught.models.Location> call, Throwable t) {

                    Log.d("RolSelectionActivity", "Error trying to connect to database");

                }
            });

        }

        private void populateGeofenceList() {

            try {
                FileInputStream fis = openFileInput("GeofenceList.der");
                ObjectInputStream is;
                is = new ObjectInputStream(fis);
                geofenceRList = (List<GeofenceR>) is.readObject();

                is.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);


        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        mService = ApiUtils.getSOService();

        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }


    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }


}
