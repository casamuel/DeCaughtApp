package escoladeltreball.org.decaught.rest;


import java.util.List;

import escoladeltreball.org.decaught.models.Contact;
import escoladeltreball.org.decaught.models.Device;
import escoladeltreball.org.decaught.models.GeofenceR;
import escoladeltreball.org.decaught.models.Location;
import escoladeltreball.org.decaught.models.Token;
import escoladeltreball.org.decaught.models.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SOService {

    // Registrar usuarios
    @FormUrlEncoded
    @POST("users/")
    Call<User> insert_user(@Field("username") String username, @Field("first_name") String first_name, @Field("last_name") String last_name, @Field("email") String email, @Field("password") String password);

    // Autenticar usuarios
    @FormUrlEncoded
    @POST("api-auth/")
    Call<Token> get_tokken(@Field("username") String username, @Field("password") String password);

    // Añadir dispositivos
    @FormUrlEncoded
    @POST("devices/")
    Call<Device> add_device(@Header("Authorization") String token, @Field("imei") String imei, @Field("label") String label, @Field("role") String role, @Field("regId") String regId);

    // Obetener dispositivos emisores
    @GET("devices/")
    //  the string in the GET is end part of the endpoint url
    Call<List<Device>> listDevices(@Header("Authorization") String token);

    // Añadir contactos
    @FormUrlEncoded
    @POST("contacts/")
    Call<Contact> add_contact(@Header("Authorization") String token, @Field("device") String device, @Field("phone") String phone, @Field("name") String name);

    // Consultar contactos
    @GET("contacts/")
    Call<List<Contact>> get_contacts(@Header("Authorization") String token, @Query("role") String role);

    // Añadir ubicaciones
    @FormUrlEncoded
    @POST("locations/")
    Call<Location> add_location(@Header("Authorization") String token, @Field("device") String device, @Field("data_time") String datatime, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("im_out") Boolean im_out);

    // Consultar ultima ubicacion
    @GET("locations/") //  the string in the GET is end part of the endpoint url
    public Call<Location> lastLocation(@Header("Authorization") String token, @Query("device") String device);

    //Añadir Zona segura
    @FormUrlEncoded
    @POST("geofences/")
    Call<GeofenceR> add_geofence(@Header("Authorization") String token, @Field("device") String device, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("radius") String radius);

    // Obetener geofences
    @GET("geofences/") //  the string in the GET is end part of the endpoint url
    public Call<List<GeofenceR>> listGeofence(@Header("Authorization") String token, @Query("device") String device);
}